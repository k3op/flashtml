#!/bin/bash
# k3op System
# FOR PRODUCTION USE ONLY

git pull

echo -e "🔫 REMOVE ALL DOCKER CONTAINERS"
# ERASE all conntainer and create a fresh env
docker container stop $(docker container ls -aq) || echo '\n✅  No containers to stop'
docker container rm $(docker container ls -aq) || echo '\n✅   No containers to remove'

echo -e "🔫 REMOVE ALL DOCKER IMAGES"
# ERASE all images
docker rmi $(docker images -a -q)
docker rmi flashtml

# BUILD flashtml ONLY
#docker build --no-cache -t flashtml .
#docker run --name flashtml -d -it -p 443:443 -p 80:80 flashtml

echo -e "⚙️ RUN PRODUCTION"

#sh init_production.sh
sh run_production.sh
