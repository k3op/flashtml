Crazy fast html container with NGINX + Pagespeed


# FILL DNS to point server IP
A record
@
MYIPADRESS


# FILL DNS to enable CAA for SSL certificate

CAA Record
@
0 issue "letsencrypt.org"

CAA Record
@
0 issuewild ";"

CAA Record
@
0 iodef "mailto:admin@keops.io"

#ENROLL & RUN (new setup)


DEPLOY manually from docker host
````
docker pull k3op/flashtml
````

##RUN flashtml on Production
```
docker run --name flashtml -d -p 80:80  -v $(pwd)/html:/var/www/html k3op/flashtml
```

##RUN flashtml on local machine (docker)
```
docker run -p 7000:80  -v $(pwd)/html:/var/www/html k3op/flashtml
```


#Special COMMANDS

**WARNING Reset infrastructure **

```
./restart.sh
```



#BUILD
======

##Build flashtml

```
docker build -t flashtml .
docker run --name flashtml -d -it -p 8081:443 -p 8080:80 flashtml
```



##SSH into flashtml container

```
docker exec -it flashtml /bin/bash
```


#DOCKER REGISTRY
================
Automatically done via bitbucket's pipelines

##LOGIN
```
docker login --username k3op --password-stdin
```

##TAG IMAGE
```
docker tag flashtml k3op/flashtml
```

##PUSH IMAGE
```
docker push k3op/flashtml
```

#DEPLOY KUBERNETES
==================

1) CREATE
Build deployment
```
kubectl create -f ./deployment.yml
```
kubectl get deployments


2) EXPOSE

#Expose minikube
```
kubectl expose deployment flashtml --type=NodePort --port=80
```

2) ACCESS

#Open service url
```
minikube service flashtml --url
```

#DEPLOY ON GCE
==============
```
docker tag flashtml gcr.io/cluster-gce/flashtml:test
```

```
gcloud docker -- push gcr.io/cluster-gce/flashtml
```

gcloud container images list

#LETSENCRYPT

docker run --rm  -it  \
  -v "$(pwd)/out":/acme.sh  \
  --net=host \
  neilpang/acme.sh  --issue -d demo.flashtml.net  --standalone

acme.sh --issue -d example.com -w /home/wwwroot/example.com


# TERMINAL theme
curl https://raw.githubusercontent.com/mashaal/wild-cherry/master/zsh/wild-cherry.zsh-theme >> .oh-my-zsh/themes/wild-cherry
