#!/bin/bash
# k3op System

docker container stop $(docker container ls -aq) || echo 'No containers to stop\n'
docker container rm $(docker container ls -aq)



docker run -p 80:80 -p 443:443 \
  --name nginx \
  -v /etc/nginx/conf.d  \
  -v /etc/nginx/vhost.d \
  -v /usr/share/nginx/html \
  -v "$(pwd)/certs:/etc/nginx/certs:ro" \
  --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \
  nginx


  docker run \
  --name nginx-gen \
  --volumes-from nginx \
  -v ./nginx.tmpl:/etc/docker-gen/templates/nginx.tmpl:ro \
  -v /var/run/docker.sock:/tmp/docker.sock:ro \
  --label com.github.jrcs.letsencrypt_nginx_proxy_companion.docker_gen \
  jwilder/docker-gen \
  -notify-sighup nginx -watch -wait 5s:30s /etc/docker-gen/templates/nginx.tmpl /etc/nginx/conf.d/default.conf

  docker run \
    --name nginx-letsencrypt \
    --volumes-from nginx \
    -v ./certs:/etc/nginx/certs:rw \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    jrcs/letsencrypt-nginx-proxy-companion


DOMAIN=flashtml.net
docker run -p 80:80 -p 443:443\
  -e "VIRTUAL_HOST=www.$DOMAIN" \
  -e "LETSENCRYPT_HOST=www.$DOMAIN" \
  -e "LETSENCRYPT_EMAIL=contact@$DOMAIN" \
  --name nginx \
  nginx
#
#
#
#
#   mkdir "$PWD"/certs
#
#
# # reverse proxy
# docker run  -p 80:80 -p 443:443 \
#   --name nginx-proxy \
#   -v "$PWD"/certs:/etc/nginx/certs:ro \
#   -v /etc/nginx/vhost.d \
#   -v /usr/share/nginx/html \
#   -v /var/run/docker.sock:/tmp/docker.sock:ro \
#   --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \
#   --restart=always \
#   jwilder/nginx-proxy
#
#
# #  LetEncrypt companion
#   docker run  \
#   --name nginx-letsencrypt \
#   -v "$PWD"/certs:/etc/nginx/certs:rw \
#   -v /var/run/docker.sock:/var/run/docker.sock:ro \
#   --volumes-from nginx-proxy \
#   --restart=always \
#   jrcs/letsencrypt-nginx-proxy-companion
