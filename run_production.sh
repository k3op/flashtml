
git pull

source .env
echo .env

echo -e "🔫 REMOVE ALL DOCKER CONTAINERS"
# ERASE all conntainer and create a fresh env
docker container stop $(docker container ls -aq) || echo '✅  No containers to stop'
docker container rm $(docker container ls -aq) || echo '✅   No containers to remove'

echo -e "🎉 COMPOSE CONTAINER"

docker-compose up -d --build

# SHOW LOGS
#docker logs --tail 50 --follow --timestamps flashtml_web_1

# RUN SSH CONNECTION
#./run_production.sh && docker exec -it flashtml_web_1 /bin/bash

# TEST NGINX CONFIGURATION
#nginx -t -c /etc/nginx/nginx.conf
