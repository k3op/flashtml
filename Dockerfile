FROM ubuntu

MAINTAINER KEOPS <contact@keops.com>
LABEL vendor="Keops Systems"

ENV LANG C.UTF-8

RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y rsync
RUN apt-get install -y wget
RUN apt-get install -y python2.7
RUN apt-get install -y gnupg



RUN apt-get install -y nginx

ADD /conf/default.conf /etc/nginx/conf.d/default.conf
ADD /conf/ssl-params.conf /etc/nginx/snippets/ssl-params.conf
ADD /conf/nginx.conf /etc/nginx/nginx.conf


# COMPILE NGINX + AMPLIFY
ADD build.sh build.sh
RUN /build.sh

# LOG NGINX forward request logs to Docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log



# Expose ports
EXPOSE 80
EXPOSE 443

# Define default command
CMD ["nginx", "-g", "daemon off;"]
