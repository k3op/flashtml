echo -e "⚙️ LOAD ENV VARS"
LANGUAGE=en_US.UTF-8
# echo $FLASH_DOMAIN
# export FLASH_DOMAIN="flashtml.net"
# echo $FLASH_DOMAIN

echo -e "⚙️ Setup LANGUAGE"

sudo sh -c "echo 'LANG=$LANGUAGE\nLANGUAGE=$LANGUAGE\n' > /etc/default/locale"
sudo sh -c "echo '
export LANGUAGE=$LANGUAGE
export LANG=$LANGUAGE
export LC_ALL=$LANGUAGE
export LC_CTYPE=$LANGUAGE
' >> $HOME/.bashrc"
source $HOME/.bashrc

echo "⚙️ Adding a bit of color and formatting to the command prompt"
echo '
export PS1="⚡️ \[\033[01;31m\]\h\[\033[01;34m\] \W \$\[\033[00m\] "
' >> $HOME/.bashrc
source $HOME/.bashrc


echo -e "⚙️ GENERATE Keychain dhparams.pem"
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

echo -e "⚙️ SET hostname"
RUN hostname $FLASH_DOMAIN

echo -e "⚙️ REMOVE ALL DOCKER CONTAINERS"
# ERASE all conntainer and create a fresh env
docker container stop $(docker container ls -aq) || echo '\n✅  No containers to stop'
docker container rm $(docker container ls -aq) || echo '\n✅   No containers to remove'

echo -e "⚙️ PREPARE acme.sh"
curl https://get.acme.sh | sh
echo "alias acme.sh='~/.acme.sh/acme.sh'" >> ~/.bashrc

echo -e "⚙️ PREPARE dhparam.pem"
openssl dhparam -out $(pwd)/certs/dhparam.pem 2048

useradd --no-create-home nginx


echo -e "⚙️ PREPARE SSL registration"
acme.sh --issue --standalone -d $FLASH_DOMAIN

echo -e "🚦 EXPORT keys to certs folder"
acme.sh --install-cert -d $FLASH_DOMAIN \
--cert-file      $(pwd)/certs/cert.pem  \
--key-file       $(pwd)/certs/key.pem  \
--fullchain-file $(pwd)/certs/fullchain.pem

curl https://rclone.org/install.sh | sudo bash


echo -e "⚙️ BACKUP SSL KEYS"
tar czvf cert_$FLASH_DOMAIN.tar.gz $(pwd)/certs
#rclone lsd n:
#rclone sync cert_$FLASH_DOMAIN.tar.gz n:backup-keops-certificates
# [n]
# type = s3
# provider = AWS
# env_auth = false
# access_key_id = AKIAIX3HHAPFZOK35YIQ
# secret_access_key = 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
# region = eu-west-2
# endpoint = backup-keops-certificates
# location_constraint = eu-west-2
# acl = private

# Inspect container
#docker exec -it flashtml_web_1 /bin/bash
