#!/bin/bash
# k3op System
# FOR DEVELOPMENT USE ONLY

echo -e "\e[1m 🚦 REMOVE ALL DOCKER CONTAINERS"
# ERASE all conntainer and create a fresh env for flashtml
docker container stop $(docker container ls -aq) || echo '\nNo containers to stop\n'
docker container rm $(docker container ls -aq)


echo -e "\e[1m 🚦 BUILD DOCKER IMAGE"
#OPTION 1 BUILD AND RUN
docker build -t flashtml .
echo -e "\e[1m 🚦 PREPARE TO RUN DOCKER IMAGE"
docker run -d  -it --name flashtml  -p 80:80 -v $(pwd)/html:/var/www/html flashtml

echo -e "\e[1m ✅ ACCESS URL"
echo -e "\e[0m "

# OPTION 2 COMPOSE
#docker-compose build
#docker-compose up -d

# kubectl create -f ./deployment.yml
# kubectl expose deployment flashtml --type=NodePort --port=80
# minikube service flashtml --url
